# ANSI escape sequences for colors
BLACK = '\033[30m'
RED = '\033[31m'
GREEN = '\033[32m'
YELLOW = '\033[33m'  # orange on some systems
BLUE = '\033[34m'
MAGENTA = '\033[35m'
CYAN = '\033[36m'
LIGHT_GRAY = '\033[37m'
DARK_GRAY = '\033[90m'
BRIGHT_RED = '\033[91m'
BRIGHT_GREEN = '\033[92m'
BRIGHT_YELLOW = '\033[93m'
BRIGHT_BLUE = '\033[94m'
BRIGHT_MAGENTA = '\033[95m'
BRIGHT_CYAN = '\033[96m'
WHITE = '\033[97m'
RESET = '\033[0m'  # called to return to standard terminal text color


# Function to color a string based on a provided pattern and color
def color_string(string, pattern, color=RED):
    return string.replace(pattern, f'{color}{pattern}{RESET}')


teststring = """
Entries containing datacatalog231:
('068dadca-3a47-4cd5-b1ba-ec0992e27846', 18759825, 17887062, 20154, 20433, '', datetime.datetime(2016, 7, 6, 10, 22, 31, 989000), datetime.datetime(2024, 5, 21, 12, 36, 4, 958000), True, 51, 0, '<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales="en_US" default-locale="en_US"><Name language-id="en_US">Data Catalogue</Name></root>', '', '', '', '', 'portlet', 'column-1=GCubeCkanDataCatalogPortlet_WAR_gcubeckan\x1b[91mdatacatalog231\x1b[0m,\nlayout-template-id=1_column\nprivateLayout=true\n', True, '/data-catalogue', False, 0, '', '', '', '', '', 6, '', False, '')
('493c3db9-add2-42bf-8883-363a8a37e030', 21058, 20677, 20154, 20433, '', datetime.datetime(2016, 6, 20, 16, 48, 40, 463000), datetime.datetime(2024, 5, 23, 12, 56, 28, 813000), True, 12, 0, '<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales="en_US" default-locale="en_US"><Name language-id="en_US">Catalogue</Name></root>', '', '', '', '', 'portlet', 'column-1=56_INSTANCE_q4wD5bGrylA7,GCubeCkanDataCatalogPortlet_WAR_gcubeckan\x1b[91mdatacatalog231\x1b[0m\nlayout-template-id=1_column\nlayoutUpdateable=true\nprivateLayout=true\n', False, '/catalogue', False, 0, 'gcuberesponsive_WAR_gcuberesponsivetheme', '', '', '', '.aui .site-navigation {\npadding-top: 10px;\n}\ndiv#wrapper {\npadding: 10px;\n}\nbody.signed-in header#banner {\nmargin-top: 50px;\n}\ndiv#heading {\ndisplay: none;\n}\n.feedback-container {\ndisplay: none;\n}', 6, '', False, '')
('b1039732-6469-4a4a-9b25-74bf45d3ca99', 18061194, 18061001, 20154, 20433, '', datetime.datetime(2016, 7, 21, 14, 32, 21, 939000), datetime.datetime(2024, 7, 4, 8, 23, 32, 406000), True, 9, 0, '<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales="en_US" default-locale="en_US"><Name language-id="en_US">GRSF PRE Records Management</Name></root>', '', '', '', '', 'portlet', 'column-1=GCubeCkanDataCatalogPortlet_WAR_gcubeckan\x1b[91mdatacatalog231\x1b[0m,GCubeCkanDataCatalogPortlet_WAR_gcubeckandatacatalog230SNAPSHOT,GCubeCkanDataCatalogPortlet_WAR_gcubeckandatacatalog,GCubeCkanDataCatalogPortlet_WAR_gcubeckandatacatalog224\ncolumn-1-customizable=false\nlayout-template-id=1_column\nlayoutUpdateable=true\nprivateLayout=true\n', False, '/data-catalogue', False, 0, 'gcuberesponsive_WAR_gcuberesponsivetheme', '', '', '', '', 3, '', False, '')
('ae04b966-96ca-41f7-8a76-ee60083b5d76', 18818638, 17887062, 20154, 21743, 'Francesco Mangiacrapa', datetime.datetime(2024, 3, 19, 11, 21, 57, 319000), datetime.datetime(2024, 7, 4, 8, 23, 56, 948000), False, 54, 0, '<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales="en_US" default-locale="en_US"><Name language-id="en_US">CatGRSF-Pre</Name></root>', '', '', '', '', 'portlet', 'column-1=GCubeCkanDataCatalogPortlet_WAR_gcubeckan\x1b[91mdatacatalog231\x1b[0m,GCubeCkanDataCatalogPortlet_WAR_gcubeckandatacatalog230SNAPSHOT\ncolumn-1-customizable=false\nlayout-template-id=1_column\nlayoutUpdateable=true\nsitemap-changefreq=daily\nsitemap-include=1\n', False, '/catalogue-grsf-pre', False, 0, 'gcuberesponsive_WAR_gcuberesponsivetheme', '', '', '', '#wrapper .landing-page1 div.row.text-header div.container {\n\tdisplay: none;\n}\n\n#wrapper div#JoinVRE-Container .row {\n    margin-left: 15px;\n    margin-right: 15px;\n}\n\n#wrapper .logo-container {\n    width: 170px;\n}\n\ndiv#gCubeCkanDataCatalog > div > div.row-fluid {\ntext-align: center;\n}\n\n\ndiv#gCubeCkanDataCatalog > div > div.row-fluid  .btn {\n    border: none;\n    color: #073763;\n}\n\n.aui .row-fluid .span3:first-child {\nmargin-left: 2.5641%;\n}', 11, '', False, '')"""

print (color_string("stringa di prova, coloriamo solo una parola", "coloriamo"))


print (color_string(teststring, "datacatalog231"))


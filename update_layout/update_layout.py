import psycopg2
import re
import os
import argparse
import traceback

DEFAULT_CONTEXT_LENGTH = 20
DEFAULT_CONFIG_FILE = "/home/life/Portal-Bundle/portal-setup-wizard.properties"

# Function to read database connection parameters from the configuration file
def read_db_config(config_file_path):
    config = {}
    with open(config_file_path, 'r') as file:
        for line in file:
            if line.strip() and '=' in line and not line.startswith("#"):
                key, value = line.strip().split('=', 1)
                config[key.strip()] = value.strip()
    return config

# Function to connect to the database
def connect_to_db(db_host, db_port, db_name, db_user, db_password):
    try:
        conn = psycopg2.connect(
            host=db_host,
            port=db_port,
            dbname=db_name,
            user=db_user,
            password=db_password
        )
        return conn
    except Exception as e:
        print(f"Error connecting to the database: {e}")
        exit(1)

# Function to update typesettings
def update_typesettings(cursor, current_pluginid, next_pluginid, log_file):
    try:
        search_query = "SELECT layoutid, typesettings FROM layout WHERE typesettings LIKE %s;"
        cursor.execute(search_query, ('%' + current_pluginid + '%',))
        rows = cursor.fetchall()

        update_query = "UPDATE layout SET typesettings = replace(typesettings, %s, %s) WHERE layoutid = %s;"
        
        for row in rows:
            layoutid, typesettings = row
            updated_typesettings = typesettings.replace(current_pluginid, next_pluginid)
            cursor.execute(update_query, (current_pluginid, next_pluginid, layoutid))
            log_file.write(f"Executed: {cursor.mogrify(update_query, (current_pluginid, next_pluginid, layoutid)).decode('utf-8')}\n")
            restore_query = "UPDATE layout SET typesettings = %s WHERE layoutid = %s;"
            log_file.write(f"Restore command: {cursor.mogrify(restore_query, (typesettings, layoutid)).decode('utf-8')}\n")
        print(f"Replaced {current_pluginid} with {next_pluginid} in typesettings.")

    except Exception as e:
        error_message = f"Error during update: {e}"
        print(error_message)
        log_file.write(f"{error_message}\n")
        log_file.write(traceback.format_exc())

# Function to search for entries containing CURRENT_PLUGINID with context
def search_current_pluginid(cursor, current_pluginid, context_size=DEFAULT_CONTEXT_LENGTH):
    try:
        search_query = "SELECT layoutid, typesettings FROM layout WHERE typesettings LIKE %s;"
        cursor.execute(search_query, ('%' + current_pluginid + '%',))
        rows = cursor.fetchall()
        print(f"Entries containing {current_pluginid}:")
        for layoutid, typesettings in rows:
            matches = [(m.start(), m.end()) for m in re.finditer(re.escape(current_pluginid), typesettings)]
            for start, end in matches:
                context_start = max(0, start - context_size)
                context_end = min(len(typesettings), end + context_size)
                context_str = typesettings[context_start:context_end]
                print(f"Layout ID: {layoutid}, Context: ...{context_str}...")
    except Exception as e:
        print(f"Error during search: {e}")
        print(traceback.format_exc())

# Parsing command-line arguments
parser = argparse.ArgumentParser(
    description='Update and search layout typesettings.',
    epilog='''
Examples of usage:

1. Update typesettings:
   python3 update_layout.py --current-pluginid CURRENT_PLUGINID --next-pluginid NEXT_PLUGINID --update

2. Search for entries with current plugin ID (default action):
   python3 update_layout.py --current-pluginid CURRENT_PLUGINID

3. Use a configuration file for database parameters:
   python3 update_layout.py --config-file /path/to/portal-setup-wizard.properties --current-pluginid CURRENT_PLUGINID
    ''',
    formatter_class=argparse.RawTextHelpFormatter
)
parser.add_argument('--config-file', default=DEFAULT_CONFIG_FILE, help="Path to the configuration file with database parameters")
parser.add_argument('--db-host', help="Database host")
parser.add_argument('--db-port', type=int, help="Database port")
parser.add_argument('--db-name', help="Database name")
parser.add_argument('--db-user', help="Database user")
parser.add_argument('--db-password', help="Database password")
parser.add_argument('--current-pluginid', '-c', type=str, required=True, help='Current plugin ID to search for')
parser.add_argument('--next-pluginid', '-n', type=str, help='Next plugin ID to replace with')
parser.add_argument('--update', '-u', action='store_true', help='Update the typesettings by replacing CURRENT_PLUGINID with NEXT_PLUGINID')
parser.add_argument('--context-size', '-s', type=int, default=DEFAULT_CONTEXT_LENGTH, help='Number of characters of context to show around the found pattern')

args = parser.parse_args()

# Initialize database connection parameters
db_host = db_port = db_name = db_user = db_password = None

# Determine the database connection parameters
if args.config_file:
    db_config = read_db_config(args.config_file)
    db_host = db_config['jdbc.default.url'].split('/')[2].split(':')[0]
    db_port = int(db_config['jdbc.default.url'].split('/')[2].split(':')[1])
    db_name = db_config['jdbc.default.url'].split('/')[-1]
    db_user = db_config['jdbc.default.username']
    db_password = db_config['jdbc.default.password']

# Override with command-line arguments if provided
db_host = args.db_host or db_host
db_port = args.db_port or db_port
db_name = args.db_name or db_name
db_user = args.db_user or db_user
db_password = args.db_password or db_password

# Check if all necessary parameters are available
if not (db_host and db_port and db_name and db_user and db_password):
    print("Database connection parameters are incomplete.")
    exit(1)

# Connect to the database
conn = connect_to_db(db_host, db_port, db_name, db_user, db_password)
cursor = conn.cursor()

log_filename = "update_log.txt"
with open(log_filename, 'a') as log_file:
    if args.update:
        update_typesettings(cursor, args.current_pluginid, args.next_pluginid, log_file)
    else:
        search_current_pluginid(cursor, args.current_pluginid, args.context_size)

    conn.commit()
    cursor.close()
    conn.close()

# update_robots.py

## Description

The update_robots.py script is designed to manage and update the false-robots.txt field in the group_ table of a PostgreSQL database used by Liferay, using a template file for definition.

It provides various functionalities, including updating the field based on a template file, listing current virtual hosts (vhosts), listing current false-robots.txt values and their associated hostnames, saving the current vhosts to individual files, and printing the differences between current and new false-robots.txt values. 

The scripts reads the db configurations from the portal-setup-wizard.properties of the instance.


## TEMPLATE FILE

the default file is template-robots.txt
keep it versioned please

## Examples of usage:

1. Use a configuration file for database parameters and print the update queries:
   python3 update_robots.py --config-file /home/life/Portal-Bundle/portal-setup-wizard.properties --template-file template-robots.txt

2. Specify database parameters directly from the command line and print the update queries:
   python3 update_robots.py --db-host postgres --db-port 5432 --db-name liferay_db --db-user infra_bundle_dev --db-password pass_db --template-file template-robots.txt

3. Execute the update queries:
   python3 update_robots.py --config-file /home/life/Portal-Bundle/portal-setup-wizard.properties --template-file template-robots.txt --execute

4. Print the list of current vhosts:
   python3 update_robots.py --config-file /home/life/Portal-Bundle/portal-setup-wizard.properties --list-vhosts

5. Print the list of current false-robots.txt values and related hostnames:
   python3 update_robots.py --config-file /home/life/Portal-Bundle/portal-setup-wizard.properties --list-robots

6. Save the current vhosts to files:
   python3 update_robots.py --config-file /home/life/Portal-Bundle/portal-setup-wizard.properties --save-vhosts --output-dir currents_robots

7. Update the false-robots.txt for a specific hostname:
   python3 update_robots.py --config-file /home/life/Portal-Bundle/portal-setup-wizard.properties --template-file template-robots.txt --hostname specific.hostname.com

8. Print the differences between current and new false-robots.txt values:
   python3 update_robots.py --config-file /home/life/Portal-Bundle/portal-setup-wizard.properties --template-file template-robots.txt --print-differences
